#ifndef UPDATEPARTDIALOG_H
#define UPDATEPARTDIALOG_H

#include <QDialog>
#include "part.h"

namespace Ui {
class UpdatePartDialog;
}

class UpdatePartDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdatePartDialog(QModelIndex index, QWidget *parent = 0);
    ~UpdatePartDialog();

    Part part;

private slots:
    void paidChecked(bool checked);
    void fillPart();

private:
    Ui::UpdatePartDialog *ui;
};

#endif // UPDATEPARTDIALOG_H
