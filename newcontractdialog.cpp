#include "newcontractdialog.h"
#include "ui_newcontractdialog.h"

#include <QDialogButtonBox>
#include <QPushButton>

NewContractDialog::NewContractDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewContractDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Adicionar");

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Cancelar");

    ui->deStart->setDate(QDate::currentDate());
    ui->deEnd->setDate(QDate::currentDate().addYears(1));

    ui->dePartStart->setDate(QDate::currentDate().addMonths(1));
    ui->dePartEnd->setDate(QDate::currentDate().addYears(1));

    this->setWindowTitle("Adicionar Contrato");

    connect(ui->leStudent, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->leSchool, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->deStart, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->deEnd, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->spDue, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->dePartStart, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->dePartEnd, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->spValue, SIGNAL(editingFinished()), this, SLOT(checkValid()));
}

NewContractDialog::~NewContractDialog()
{
    delete ui;
}

Contract NewContractDialog::contract() const
{
    Contract c;

    c.student = ui->leStudent->text();
    c.school = ui->leSchool->text();
    c.start = ui->deStart->date();
    c.end = ui->deEnd->date();

    c.obs = ui->leObs->text();

    c.due = ui->spDue->value();

    c.partStart.setDate(ui->dePartStart->date().year(), ui->dePartStart->date().month(), c.due);
    c.partEnd.setDate(ui->dePartEnd->date().year(), ui->dePartEnd->date().month(), c.due);

    c.value = ui->spValue->value();

    return c;
}

void NewContractDialog::checkValid() const
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(contract().isValid());
}
