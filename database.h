#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QModelIndex>

#include "contract.h"
#include "part.h"

class QAbstractItemModel;
class QSqlTableModel;
class QSqlQueryModel;
class QSqlQuery;

class Database
{
public:
    static Database *Instance();
    static void Finalize();

    QAbstractItemModel* contracts() const;

    void addContract(const Contract &contract);
    Contract getContract(QModelIndex index);
    void updateContract(QModelIndex index, const Contract &contract);
    void finalizeContract(QModelIndex index);
    void removeContract(QModelIndex index);

    void justActive();
    void justFinished();
    void showAll();
    void filterStudent(QString string);

    QAbstractItemModel* parts() const;

    Part getPart(QModelIndex index);
    void updatePart(QModelIndex index, const Part &contract);

    void justLate();
    void justPayed();
    void showAllParts();
    void filterPart(QString string);
    void filterPartDate(QDate start, QDate end);

    void refreshParts();

    float partSum();

private:
    Database();
    ~Database();

    static Database *instance;

    QSqlQuery getPartsQuery();

    QSqlDatabase db;
    QSqlTableModel *contractsModel;
    QSqlQueryModel *partsModel;

    QString contractsStatusFilter, contractsNameFilter;
    QString partsDateFilter, partsNameFilter, partsStatusFilter;
};

#endif // DATABASE_H
