#ifndef NEWCONTRACTDIALOG_H
#define NEWCONTRACTDIALOG_H

#include <QDialog>
#include "contract.h"

namespace Ui {
class NewContractDialog;
}

class NewContractDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewContractDialog(QWidget *parent = 0);
    ~NewContractDialog();

    Contract contract() const;

private slots:
    void checkValid() const;

private:
    Ui::NewContractDialog *ui;
};

#endif // NEWCONTRACTDIALOG_H
