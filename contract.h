#ifndef CONTRACT_H
#define CONTRACT_H

#include <QString>
#include <QDate>

class Contract
{
public:
    Contract();
    ~Contract();

    QString student;
    QString school;
    QDate start;
    QDate end;

    QString obs;

    QDate partStart;
    QDate partEnd;
    int due;

    float value;

    bool isValid() const;
};

#endif // CONTRACT_H
