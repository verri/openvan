#include "database.h"

#include <QSqlQueryModel>
#include <QMessageLogger>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlTableModel>
#include <QSqlRecord>

Database *Database::instance = 0;

Database *Database::Instance()
{
    if (!instance)
        instance = new Database;
    return instance;
}

void Database::Finalize()
{
    if (instance) {
        delete instance;
        instance = 0;
    }
}

Database::Database() :
    contractsStatusFilter("1 == 1"),
    contractsNameFilter("1 == 1"),
    partsDateFilter("1 == 1"),
    partsNameFilter("1 == 1"),
    partsStatusFilter("1 == 1")
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("db.sqlite");
    if (!db.open())
        qFatal("Error opening database.");

    const QString initStatements =
        "CREATE TABLE IF NOT EXISTS Contract ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "student TEXT NOT NULL,"
            "school TEXT NOT NULL,"
            "start TEXT NOT NULL,"
            "end TEXT NOT NULL,"
            "obs TEXT,"

            "CHECK (end > start)"
        ");"

        "CREATE TABLE IF NOT EXISTS Part ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "contract INTEGER NOT NULL,"
            "value REAL NOT NULL,"
            "due TEXT NOT NULL,"

            "ignore INTEGER NOT NULL DEFAULT 0,"
            "receivedvalue REAL NOT NULL DEFAULT 0,"
            "receiveddate TEXT,"

            "CHECK (value > 0),"
            "UNIQUE (contract, due),"
            "FOREIGN KEY (contract) REFERENCES Contract(id) ON DELETE CASCADE"
        ");"

        "PRAGMA foreign_keys = ON;";



    QStringList statements = initStatements.split(";");

    foreach(QString statement, statements) {
        if (statement.trimmed().isEmpty())
            continue;

        QSqlQuery query(db);
        query.prepare(statement);

        qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());

        if (!query.exec())
            qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());
    }

    contractsModel = new QSqlTableModel(0, db);
    contractsModel->setTable("Contract");
    contractsModel->select();
    contractsModel->setHeaderData(1, Qt::Horizontal, "Aluno");
    contractsModel->setHeaderData(2, Qt::Horizontal, "Escola");
    contractsModel->setHeaderData(3, Qt::Horizontal, "Início");
    contractsModel->setHeaderData(4, Qt::Horizontal, "Fim");
    contractsModel->setHeaderData(5, Qt::Horizontal, "Observações");

    QSqlQuery partsQuery = getPartsQuery();

    partsModel = new QSqlQueryModel();
    partsModel->setQuery(partsQuery);

    partsModel->setHeaderData(1, Qt::Horizontal, "Aluno");
    partsModel->setHeaderData(2, Qt::Horizontal, "Escola");
    partsModel->setHeaderData(3, Qt::Horizontal, "Vencimento");
    partsModel->setHeaderData(4, Qt::Horizontal, "Valor");
    partsModel->setHeaderData(5, Qt::Horizontal, "Recebido");
    partsModel->setHeaderData(6, Qt::Horizontal, "Data");
}

Database::~Database()
{
    qDebug("Closing db.");

    db.close();
    delete contractsModel;
    delete partsModel;
}

QAbstractItemModel *Database::contracts() const
{
    return contractsModel;
}

QAbstractItemModel *Database::parts() const
{
    return partsModel;
}

Part Database::getPart(QModelIndex index)
{
    Part part;

    int row = index.row();

    index = contractsModel->index(row, 1);
    part.student = index.data().toString();

    index = partsModel->index(row, 2);
    part.school = index.data().toString();

    index = partsModel->index(row, 3);
    part.due = QDate::fromString(index.data().toString(), "dd/MM/yyyy");

    index = partsModel->index(row, 4);
    part.value = index.data().toFloat();

    index = partsModel->index(row, 5);
    part.receivedValue = index.data().toFloat();

    index = partsModel->index(row, 6);
    part.receivedDate = QDate::fromString(index.data().toString(), "dd/MM/yyyy");

    index = partsModel->index(row, 7);
    part.ignore = index.data().toBool();

    return part;
}

void Database::updatePart(QModelIndex index, const Part &part)
{
    int row = index.row();
    index = partsModel->index(row, 0);
    int id = index.data().toInt();

    QString statement;

    if (!part.ignore && part.receivedValue > 0) {
        statement = QString("UPDATE Part SET receivedvalue = %1, receiveddate = '%2', ignore = 0 "
                            "WHERE id = %3")
                            .arg(part.receivedValue)
                            .arg(part.receivedDate.toString("dd/MM/yyyy"))
                            .arg(id);
    } else {
        statement = QString("UPDATE Part SET receivedvalue = 0, receiveddate = NULL, ignore = %1 "
                            "WHERE id = %2")
                            .arg(part.ignore ? 1 : 0)
                            .arg(id);
    }

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());
}

void Database::addContract(const Contract &contract)
{
    if (!contract.isValid()) {
        qFatal("Impossible invalid contract.");
        return;
    }

    QString statement = QString("INSERT INTO Contract(student, school, start, end, obs) "
                                "VALUES (\"%1\", \"%2\", \"%3\", \"%4\", \"%5\")")
                        .arg(contract.student)
                        .arg(contract.school)
                        .arg(contract.start.toString("dd/MM/yyyy"))
                        .arg(contract.end.toString("dd/MM/yyyy"))
                        .arg(contract.obs);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    int id = query.lastInsertId().toInt();

    QDate dueDate = contract.partStart;

    while (dueDate <= contract.partEnd) {

        statement = QString("INSERT INTO Part(contract, value, due) "
                            "VALUES (%1, %2, \"%3\")")
                .arg(id)
                .arg(contract.value)
                .arg(dueDate.toString("dd/MM/yyyy"));

        query = QSqlQuery(db);
        query.prepare(statement);

        qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
        if (!query.exec())
            qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

        dueDate = dueDate.addMonths(1);
    }

    contractsModel->select();
}

Contract Database::getContract(QModelIndex index)
{
    Contract contract;

    int row = index.row();

    //index = contractsModel->index(row, 0);
    //int id = index.data().toInt();

    index = contractsModel->index(row, 1);
    contract.student = index.data().toString();

    index = contractsModel->index(row, 2);
    contract.school = index.data().toString();

    index = contractsModel->index(row, 3);
    contract.start = QDate::fromString(index.data().toString(), "dd/MM/yyyy");

    index = contractsModel->index(row, 4);
    contract.end = QDate::fromString(index.data().toString(), "dd/MM/yyyy");

    index = contractsModel->index(row, 5);
    contract.obs = index.data().toString();

    return contract;
}

void Database::updateContract(QModelIndex index, const Contract &contract)
{
    int row = index.row();

    index = contractsModel->index(row, 1);
    contractsModel->setData(index, contract.student);

    index = contractsModel->index(row, 2);
    contractsModel->setData(index, contract.school);

    index = contractsModel->index(row, 3);
    contractsModel->setData(index, contract.start.toString("dd/MM/yyyy"));

    index = contractsModel->index(row, 4);
    contractsModel->setData(index, contract.end.toString("dd/MM/yyyy"));

    index = contractsModel->index(row, 5);
    contractsModel->setData(index, contract.obs);

    contractsModel->submitAll();
}

void Database::finalizeContract(QModelIndex index)
{
    int row = index.row();
    index = contractsModel->index(row, 4);
    contractsModel->setData(index, QDate::currentDate().toString("dd/MM/yyyy"));

    int contract = contractsModel->index(row, 0).data().toInt();

    QString statement = QString("UPDATE Part SET ignore = 1 WHERE contract = %1 AND "
                                "date(substr(due, 7)||'-'||substr(due, 4, 2)||'-'||substr(due, 1, 2)) > date('now')")
            .arg(contract);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    contractsModel->submitAll();
}

void Database::removeContract(QModelIndex index)
{
    int row = index.row();
    int contract = contractsModel->index(row, 0).data().toInt();

    QString statement = QString("DELETE FROM Contract WHERE id = %1").arg(contract);

    QSqlQuery query(db);
    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    contractsModel->submitAll();
    contractsModel->select();
}

void Database::justActive()
{
    contractsStatusFilter = "date('now') >= date(substr(start, 7)||'-'||substr(start, 4, 2)||'-'||substr(start, 1, 2)) AND "
                            "date('now') <= date(substr(end, 7)||'-'||substr(end, 4, 2)||'-'||substr(end, 1, 2))";

    contractsModel->setFilter(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter));
    qDebug(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter).toStdString().c_str());
}

void Database::justFinished()
{
    contractsStatusFilter = "date('now') > date(substr(end, 7)||'-'||substr(end, 4, 2)||'-'||substr(end, 1, 2))";

    contractsModel->setFilter(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter));
}

void Database::showAll()
{
    contractsStatusFilter = "1 == 1";

    contractsModel->setFilter(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter));
}

void Database::filterStudent(QString string)
{
    if (string.isEmpty())
        contractsNameFilter = "1 == 1";
    else
        contractsNameFilter = QString("student LIKE '\%%1\%' OR school LIKE '\%%1\%'").arg(string);
    contractsModel->setFilter(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter));
    qDebug(QString("(%1) AND (%2)").arg(contractsNameFilter).arg(contractsStatusFilter).toStdString().c_str());
}

QSqlQuery Database::getPartsQuery()
{
    QSqlQuery query(db);

    QString statement = QString("SELECT Part.id, student, school, due, value, receivedvalue, receiveddate, ignore "
                                "FROM Contract, Part WHERE "
                                "Contract.id = Part.contract AND (%1) AND (%2) AND (%3)")
            .arg(partsNameFilter)
            .arg(partsStatusFilter)
            .arg(partsDateFilter);

    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    return query;
}

void Database::refreshParts()
{
    partsModel->setQuery(getPartsQuery());
}

float Database::partSum()
{
    QSqlQuery query(db);

    QString statement = QString("SELECT sum(receivedvalue) FROM Contract, Part WHERE "
                                "Contract.id = Part.contract AND (%1) AND (%2) AND (%3)")
            .arg(partsNameFilter)
            .arg(partsStatusFilter)
            .arg(partsDateFilter);

    query.prepare(statement);

    qDebug("Exec query [[\n%s\n]]", statement.toStdString().c_str());
    if (!query.exec())
        qFatal("Invalid query: %s", query.lastError().text().toStdString().c_str());

    int ix = query.record().indexOf("sum(receivedvalue)");
    query.next();
    return query.value(ix).toFloat();
}

void Database::justLate()
{
    partsStatusFilter = QString("date(substr(due, 7)||'-'||substr(due, 4, 2)||'-'||substr(due, 1, 2)) < date('now') AND "
                                "value > receivedvalue AND ignore = 0");
    refreshParts();
}

void Database::justPayed()
{
    partsStatusFilter = QString("receivedvalue > 0 AND ignore = 0");
    refreshParts();
}

void Database::showAllParts()
{
    partsStatusFilter = QString("1 == 1");
    refreshParts();
}

void Database::filterPart(QString string)
{
    if (string.isEmpty())
        partsNameFilter = "1 == 1";
    else
        partsNameFilter = QString("student LIKE '\%%1\%' OR school LIKE '\%%1\%'").arg(string);

    refreshParts();
}

void Database::filterPartDate(QDate start, QDate end)
{
    partsDateFilter = QString("date(substr(due, 7)||'-'||substr(due, 4, 2)||'-'||substr(due, 1, 2)) >= date('%1') AND "
                              "date(substr(due, 7)||'-'||substr(due, 4, 2)||'-'||substr(due, 1, 2)) <= date('%2')")
            .arg(start.toString("yyyy-MM-dd"))
            .arg(end.toString("yyyy-MM-dd"));
    refreshParts();
}
