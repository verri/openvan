#include "updatecontractdialog.h"
#include "ui_updatecontractdialog.h"

#include <QMessageBox>
#include "database.h"

UpdateContractDialog::UpdateContractDialog(QModelIndex index, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateContractDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Atualizar");

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Cancelar");

    Database *database = Database::Instance();

    contract = database->getContract(index);

    ui->leStudent->setText(contract.student);
    ui->leSchool->setText(contract.school);
    ui->leObs->setText(contract.obs);
    ui->deStart->setDate(contract.start);
    ui->deEnd->setDate(contract.end);

    ui->pbFinish->setEnabled(contract.start < QDate::currentDate());
    ui->pbFinish->setVisible(false);

    this->setWindowTitle("Editar Contrato");

    connect(ui->leStudent, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->leSchool, SIGNAL(editingFinished()), this, SLOT(checkValid()));

    connect(this, SIGNAL(accepted()), this, SLOT(checkValid()));

    connect(ui->pbFinish, SIGNAL(clicked()), this, SLOT(finish()));
    connect(ui->pbRemove, SIGNAL(clicked()), this, SLOT(remove()));
    connect(ui->deStart, SIGNAL(editingFinished()), this, SLOT(checkValid()));
    connect(ui->deEnd, SIGNAL(editingFinished()), this, SLOT(checkValid()));
}

UpdateContractDialog::~UpdateContractDialog()
{
    delete ui;
}

void UpdateContractDialog::checkValid()
{
    contract.student = ui->leStudent->text();
    contract.school = ui->leSchool->text();
    contract.obs = ui->leObs->text();
    contract.start = ui->deStart->date();
    contract.end = ui->deEnd->date();

    bool enabled = !contract.student.isEmpty() && !contract.school.isEmpty() && contract.start < contract.end;

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(enabled);
    ui->pbFinish->setEnabled(contract.start < QDate::currentDate());
}

void UpdateContractDialog::finish()
{
    QMessageBox msgBox;
    msgBox.setText("Se você finalizar este contrato, todas as parcelas não pagas serão ignoradas. "
                   "Esta operação não pode ser desfeita.");
    msgBox.setInformativeText("Deseja realmente finalizar esse contrato?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.button(QMessageBox::Ok)->setText("Finalizar!");
    msgBox.button(QMessageBox::Cancel)->setText("Cancelar");

    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    this->done(2);
}

void UpdateContractDialog::remove()
{
    QMessageBox msgBox;
    msgBox.setText("Se você remover este contrato todas as parcelas serão removidas. "
                   "Esta operação não pode ser desfeita.");
    msgBox.setInformativeText("Deseja realmente remover esse contrato?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.button(QMessageBox::Ok)->setText("Remover (cuidado)!");
    msgBox.button(QMessageBox::Cancel)->setText("Cancelar");

    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
        return;

    this->done(3);
}
