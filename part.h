#ifndef PART_H
#define PART_H

#include <QString>
#include <QDate>

class Part
{
public:
    Part();
    ~Part();

    QString student;
    QString school;

    QDate due;
    float value;

    QDate receivedDate;
    float receivedValue;

    bool ignore;
};

#endif // PART_H
