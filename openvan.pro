#-------------------------------------------------
#
# Project created by QtCreator 2015-02-22T10:00:24
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = openvan
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    contractwidget.cpp \
    newcontractdialog.cpp \
    contract.cpp \
    updatecontractdialog.cpp \
    partwidget.cpp \
    updatepartdialog.cpp \
    part.cpp

HEADERS  += mainwindow.h \
    database.h \
    contractwidget.h \
    newcontractdialog.h \
    contract.h \
    updatecontractdialog.h \
    partwidget.h \
    updatepartdialog.h \
    part.h

FORMS    += mainwindow.ui \
    contractwidget.ui \
    newcontractdialog.ui \
    updatecontractdialog.ui \
    partwidget.ui \
    updatepartdialog.ui
