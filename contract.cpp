#include "contract.h"

Contract::Contract()
{

}

Contract::~Contract()
{

}

bool Contract::isValid() const
{
    return student != "" && school != "" && start < end &&
           due > 0 && due < 29 && partStart < partEnd &&
           value > 0;
}
