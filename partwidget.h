#ifndef PARTWIDGET_H
#define PARTWIDGET_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
class PartWidget;
}

class PartWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PartWidget(QWidget *parent = 0);
    ~PartWidget();


public slots:
    void refreshView();
    void updatePart(QModelIndex index);

    void changeFilter(QString string);
    void changeFilter(int i);
    void changeFilter();

private:
    Ui::PartWidget *ui;
};

#endif // PARTWIDGET_H
