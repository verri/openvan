#ifndef UPDATECONTRACTDIALOG_H
#define UPDATECONTRACTDIALOG_H

#include <QDialog>
#include <QModelIndex>
#include "contract.h"

namespace Ui {
class UpdateContractDialog;
}

class UpdateContractDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateContractDialog(QModelIndex index, QWidget *parent = 0);
    ~UpdateContractDialog();

    Contract contract;

private slots:
    void checkValid();

    void remove();
    void finish();

private:
    Ui::UpdateContractDialog *ui;
};

#endif // UPDATECONTRACTDIALOG_H
