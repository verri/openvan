#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "database.h"
#include "contractwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabChanged(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
    Database::Finalize();
}

void MainWindow::tabChanged(int i)
{
    if (i == 1)
        ui->partWidget->refreshView();
}
