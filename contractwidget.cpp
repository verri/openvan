#include "contractwidget.h"
#include "ui_contractwidget.h"

#include <QMessageBox>
#include <QShortcut>
#include <QModelIndex>

#include "newcontractdialog.h"
#include "updatecontractdialog.h"
#include "database.h"

ContractWidget::ContractWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ContractWidget)
{
    ui->setupUi(this);

    Database *db = Database::Instance();

    ui->tableView->setModel(db->contracts());
    ui->tableView->show();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->tableView->hideColumn(0);

    QShortcut *shortcut = new QShortcut(QKeySequence("Ctrl+N"), this);

    ui->cbFilter->addItem("Ativos");
    ui->cbFilter->addItem("Todos");
    ui->cbFilter->addItem("Finalizados");
    db->justActive();

    QRegExp re("^[A-Za-z]*$");
    QRegExpValidator *validator = new QRegExpValidator(re, 0);
    ui->leFilter->setValidator(validator);

    connect(shortcut, SIGNAL(activated()), ui->pbAdd, SLOT(click()));
    connect(ui->pbAdd, SIGNAL(clicked()), this, SLOT(addContract()));
    connect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(updateContract(QModelIndex)));
    connect(ui->cbFilter, SIGNAL(currentIndexChanged(int)), this, SLOT(changeFilter(int)));
    connect(ui->leFilter, SIGNAL(textEdited(QString)), this, SLOT(changeFilter(QString)));
}

ContractWidget::~ContractWidget()
{
    delete ui;
}

void ContractWidget::addContract()
{
    NewContractDialog dialog;

    int code = dialog.exec();

    if (code == QDialog::Rejected)
        return;

    Contract contract = dialog.contract();

    if (!contract.isValid()) {
        qFatal("Impossible invalid contract.");
        return;
    }

    Database *db = Database::Instance();
    db->addContract(contract);

    //ui->tableView->setModel(db->contracts());
}

void ContractWidget::updateContract(QModelIndex index)
{
    UpdateContractDialog dialog(index);

    int code = dialog.exec();

    if (code == QDialog::Rejected)
        return;

    if (code == QDialog::Accepted)
        Database::Instance()->updateContract(index, dialog.contract);

    if (code == 2)
        Database::Instance()->finalizeContract(index);

    if (code == 3)
        Database::Instance()->removeContract(index);
}

void ContractWidget::changeFilter(int i)
{
    if (i == 0) // Ativos
        Database::Instance()->justActive();
    else if (i == 1) // Todos
        Database::Instance()->showAll();
    else if (i == 2) // Finalizados
        Database::Instance()->justFinished();
}

void ContractWidget::changeFilter(QString string)
{
    Database::Instance()->filterStudent(string);
}
