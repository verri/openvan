#include "partwidget.h"
#include "ui_partwidget.h"

#include "database.h"
#include "updatepartdialog.h"

PartWidget::PartWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PartWidget)
{
    ui->setupUi(this);

    ui->deStart->setDate(QDate::currentDate().addYears(-1));
    ui->deEnd->setDate(QDate::currentDate().addMonths(1));

    Database *db = Database::Instance();

    ui->tableView->setModel(db->parts());
    ui->tableView->show();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(7);

    ui->cbFilter->addItem("Vencidos");
    ui->cbFilter->addItem("Todos");
    ui->cbFilter->addItem("Recebidos");
    db->justLate();
    db->filterPartDate(ui->deStart->date(), ui->deEnd->date());

    QRegExp re("^[A-Za-z]*$");
    QRegExpValidator *validator = new QRegExpValidator(re, 0);
    ui->leFilter->setValidator(validator);

    ui->lStatus->setText(QString("Total recebido no período: %1").arg(db->partSum()));

    connect(ui->cbFilter, SIGNAL(currentIndexChanged(int)), this, SLOT(changeFilter(int)));
    connect(ui->leFilter, SIGNAL(textEdited(QString)), this, SLOT(changeFilter(QString)));
    connect(ui->deStart, SIGNAL(editingFinished()), this, SLOT(changeFilter()));
    connect(ui->deEnd, SIGNAL(editingFinished()), this, SLOT(changeFilter()));

    connect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(updatePart(QModelIndex)));
}

PartWidget::~PartWidget()
{
    delete ui;
}

void PartWidget::refreshView()
{
    Database *db = Database::Instance();
    db->refreshParts();

    ui->lStatus->setText(QString("Total recebido no período: %1").arg(db->partSum()));

    qDebug("Refreshing parts...");
}

void PartWidget::changeFilter(int i)
{
    Database *db = Database::Instance();

    if (i == 0) // Ativos
        db->justLate();
    else if (i == 1) // Todos
        db->showAllParts();
    else if (i == 2) // Finalizados
        db->justPayed();

    ui->lStatus->setText(QString("Total recebido no período: %1").arg(db->partSum()));
}

void PartWidget::changeFilter()
{
    Database *db = Database::Instance();
    db->filterPartDate(ui->deStart->date(), ui->deEnd->date());

    ui->lStatus->setText(QString("Total recebido no período: %1").arg(db->partSum()));
}

void PartWidget::changeFilter(QString string)
{
    Database *db = Database::Instance();
    db->filterPart(string);

    ui->lStatus->setText(QString("Total recebido no período: %1").arg(db->partSum()));
}

void PartWidget::updatePart(QModelIndex index)
{
    UpdatePartDialog dialog(index);

    int code = dialog.exec();

    if (code == QDialog::Rejected)
        return;

    if (code == QDialog::Accepted)
        Database::Instance()->updatePart(index, dialog.part);

    refreshView();
}
