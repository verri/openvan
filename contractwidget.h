#ifndef CONTRACTWIDGET_H
#define CONTRACTWIDGET_H

#include <QWidget>
#include <QModelIndex>

namespace Ui {
class ContractWidget;
}

class ContractWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ContractWidget(QWidget *parent = 0);
    ~ContractWidget();

private slots:
    void addContract();
    void updateContract(QModelIndex index);

    void changeFilter(QString string);
    void changeFilter(int i);

private:
    Ui::ContractWidget *ui;
};

#endif // CONTRACTWIDGET_H
