#include "updatepartdialog.h"
#include "ui_updatepartdialog.h"

#include "database.h"
#include <QPushButton>

UpdatePartDialog::UpdatePartDialog(QModelIndex index, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdatePartDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Atualizar");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Cancelar");

    Database *database = Database::Instance();

    part = database->getPart(index);

    ui->leStudent->setText(part.student);
    ui->leSchool->setText(part.school);

    ui->deDue->setDate(part.due);
    ui->spValue->setValue(part.value);

    ui->deReceived->setDate(QDate::currentDate());
    ui->spReceived->setValue(part.value);

    if (part.ignore) {
        ui->rbIgnore->setChecked(true);
    } else if (part.receivedValue > 0) {
        ui->rbPaid->setChecked(true);
        ui->deReceived->setDate(part.receivedDate);
        ui->spReceived->setValue(part.receivedValue);
        ui->deReceived->setEnabled(true);
        ui->spReceived->setEnabled(true);
    } else {
        ui->rbPending->setChecked(true);
    }

    this->setWindowTitle("Editar Parcela");

    // connect(ui->rbPaid, SIGNAL(toggled(bool)), ui->deReceived, SLOT(setEnabled(bool)));
    // connect(ui->rbPaid, SIGNAL(toggled(bool)), ui->spReceived, SLOT(setEnabled(bool)));
    connect(ui->rbPaid, SIGNAL(toggled(bool)), this, SLOT(paidChecked(bool)));
    connect(this, SIGNAL(accepted()), this, SLOT(fillPart()));
}

UpdatePartDialog::~UpdatePartDialog()
{
    delete ui;
}

void UpdatePartDialog::paidChecked(bool checked)
{
    ui->deReceived->setEnabled(checked);
    ui->spReceived->setEnabled(checked);
}

void UpdatePartDialog::fillPart()
{
    if (ui->rbPending->isChecked()) {
        part.receivedValue = 0;
        part.ignore = false;
    } else if (ui->rbPaid->isChecked()) {
        part.receivedValue = ui->spReceived->value();
        part.receivedDate = ui->deReceived->date();
        part.ignore = false;
    } else if (ui->rbIgnore->isChecked()) {
        part.ignore = true;
        part.receivedValue = 0;
    }
}
